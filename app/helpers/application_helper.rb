module ApplicationHelper

  def question_type_options(sel=0)
    options_for_select(
        [[Question::ANSWER_TITLE_TYPE[Question::ONE_ANSWER], Question::ONE_ANSWER],
         [Question::ANSWER_TITLE_TYPE[Question::MULTI_ANSWER], Question::MULTI_ANSWER]], sel)
  end

  def get_module_name
    controller.class.name.split('::').first
  end

end
