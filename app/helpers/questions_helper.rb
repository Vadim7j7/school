module QuestionsHelper

  def status_panel_answer(quest, question)
    answer = Answer.where(quest: quest, question: question, user: current_user).pluck(:answer).last

    unless answer
      'default'
    else
      (answer == question.answer ? 'success' : 'danger')
    end
  end

  def me_answer(quest, question)
    Answer.where(quest: quest, question: question, user: current_user).pluck(:answer).last
  end

  def author_link(question)
    user = question.user
    "#{user.first_name} #{user.last_name[0]}."
  end

  def type_question(question)
    subject = question.subject
    "#{subject.name}: #{question.title_question_type}"
  end

  def multiple_questions(question, my_answer)
    result = ''
    if question.question_type == Question::MULTI_ANSWER
      result = '<ul class="list-group" id="list-options">'

      question.question_options.each_with_index do |question_option, index|
        result << "<li class=\"list-group-item\"><span class=\"color-grey\">#{Question::LIST_OPTIONS[index]}: </span> #{question_option.caption}" # Add row option
        if current_user && my_answer.blank? && (question.user != current_user) && (current_user.role == User::ROLE_STUDENT) && (question.question_type == Question::MULTI_ANSWER) # Check is answer current user
          result << "<span class=\"right\"><input type=\"radio\" name=\"question_option\" value=\"#{index}\"></span>"
        end
        result << '</li>'
      end
      result << '</ul>'
    end

    result.html_safe
  end

  def next_link_question(question)
    next_question = (Question.visible.where('id > ?', question.id).order('id ASC').first || Question.visible.first)
    "/questions/#{next_question.id}"
  end

  def answer_info(question, my_answer)
    if current_user && (question.user != current_user) && (current_user.role == User::ROLE_STUDENT)
      result = '<div class="form-inline left"><div class="form-group" id="answer_question_wr">'
      if my_answer
        result << "<span>My answer: </span><span class=\"label label-#{my_answer.right_answer ? 'success' : 'danger'}\">#{Question::title_answer(my_answer.answer, question.question_type) } #{my_answer.right_answer ? question.assessment : '0'}</span>"
      else
        if question.question_type == Question::ONE_ANSWER
          result << '<input type="text" class="form-control" id="answer_question_input" placeholder="Answer">&nbsp;&nbsp;'
        end
        result << link_to('Reply', 'javascript:void(0);',
                          :class => 'btn btn-success', :id => 'send_answer_question',
                          'data-id' => question.id, 'data-type' => question.question_type)
      end
      result << '</div></div>'

      result.html_safe
    end
  end

end
