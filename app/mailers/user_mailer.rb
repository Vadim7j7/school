class UserMailer < ApplicationMailer
  before_action :run_mailer

  def new_teacher(teacher, password)
    @teacher = teacher
    @password = password

    mail(to: @teacher.email, subject: 'Add in teachers')
  end

  private
  def run_mailer
    @host = ActionMailer::Base.default_url_options[:host]
  end

end
