class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def teacher?
    if !current_user || current_user.role != User::ROLE_TEACHER
      redirect_to root_path
      false
    end
  end

  def student?
    if !current_user || current_user.role != User::ROLE_STUDENT
      redirect_to root_path
      false
    end
  end

  def error_404
    render file: "#{Rails.root}/public/404", layout: false, status: :not_found
    false
  end

end
