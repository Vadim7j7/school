class QuestsController < ApplicationController
  before_action :authenticate_user!
  before_action :teacher?, only: [:new, :create]

  def index
    @layout_title = 'Quests'
    @quests = Quest.order(:created_at).reverse_order.all
  end

  def show
    @quest = Quest.where(id: params[:id]).last
    unless @quest
      error_404
      return false
    end

    @quest_finished = QuestFinished.where(user: current_user, quest: @quest).last

    if current_user.role == User::ROLE_STUDENT
      unless @quest_finished
        session[:quest_run] = { quest_id: @quest.id, quest_end_time: nil, current_question: 1 }
      else
        @questions = @quest.questions
      end
    end
  end

  def new
    @quest = Quest.new
    @quest.questions.build
    @quest.questions.last.question_options.build(test_answer: 1)
  end

  def create
    @quest = Quest.new(par_quest)
    @quest.user = current_user

    if @quest.save
      redirect_to quest_show_path(@quest)
    else
      render :new
    end
  end

  private

  def par_quest
    params.require(:quest).permit([:caption,
                                   :time_quest,
                                   :description,
                                   :test_answer,
                                   { questions_attributes: [:txt,
                                                            :question_type,
                                                            :answer,
                                                            :test_answer,
                                                            { question_options_attributes: [:txt] }] }])
  end

end
