class ProcessController < ApplicationController
  def run
    current_time = DateTime.now

    if session[:quest_run] && session[:quest_run]['quest_id']
      quest_id = session[:quest_run]['quest_id'].to_i

      unless QuestFinished.where(user: current_user, quest: quest_id).last
        @quest = Quest.where(id: quest_id).last


        puts session[:quest_run]

        if session[:quest_run]['quest_end_time'] == nil
          session[:quest_run]['quest_end_time'] = (current_time + @quest.time_quest.minute)
        end
      else
        redirect_to process_quest_finished_path
        return true
      end
    else
      error_404
      return false
    end

    # If answer on get next question and write answer
    answer_process

    # Get question
    g_question

    # End quest
    unless @question
      answer_count = Answer.where(user: current_user, quest: @quest).count
      answers_true = Answer.where(user: current_user, quest: @quest, true_or_false: true).count
      answers_false = Answer.where(user: current_user, quest: @quest, true_or_false: false).count

      percentage_true = ((answers_true.to_f / answer_count.to_f) * 100).to_i
      percentage_false = ((answers_false.to_f / answer_count.to_f) * 100).to_i

      a = (session[:quest_run]['quest_end_time'].to_datetime - current_time)
      time_spent = (@quest.time_quest - ((a * 24 * 60).to_i)).to_i

      session.delete(:quest_run)
      QuestFinished.create({ user: current_user,
                             quest: @quest,
                             time_spent: time_spent,
                             percentage_true: percentage_true,
                             percentage_false: percentage_false })

      redirect_to process_quest_finished_path
    end
  end

  def quest_finished
  end

  private

  def par_answer
  end

  def g_question
    @question = @quest.questions.order(:id).limit(1).offset((session[:quest_run]['current_question'].to_i - 1)).last
  end

  def answer_process
    @answer = Answer.new

    if params[:answer] && params[:answer][:answer]
      question = g_question
      @answer.answer = params[:answer][:answer]
      @answer.user = current_user
      @answer.quest = @quest
      @answer.question = question
      @answer.true_or_false = (question.answer == @answer.answer.downcase)
      @answer.save

      session[:quest_run]['current_question'] += 1
      @answer.answer = ''
    end

  end

end

