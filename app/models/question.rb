class Question < ActiveRecord::Base

  # Attributes :::::::::::::::::::::::::::::::::::::::::::::::::::::
  attr_accessor :test_answer
  before_save :question_save

  ONE_ANSWER = 0
  MULTI_ANSWER = 1
  ANSWER_TITLE_TYPE = { ONE_ANSWER => 'Question answer',
                        MULTI_ANSWER  => 'Multiple replies' }

  LIST_OPTIONS = %w(A B C D E F G)

  # Associations :::::::::::::::::::::::::::::::::::::::::::::::::
  belongs_to :user
  has_many :student_answers, dependent: :destroy
  has_many :question_options
  accepts_nested_attributes_for :question_options,
                                reject_if: :all_blank,
                                allow_destroy: true

  has_attached_file :image, styles: { thumb: '300x300#', mini: '100x100#' }

  # Validates :::::::::::::::::::::::::::::::::::::::::::::::::::
  validates_attachment_content_type :image,
                                    content_type: %w(image/jpg image/jpeg image/png image/gif)

  validates :question_options,
            length: { minimum: 2, too_short: '%{count} option minimum' },
            if: Proc.new { |qu| qu.question_type == MULTI_ANSWER }

  validates :txt, presence: true
  validates :answer, presence: true, if: Proc.new { |qu| qu.question_type == ONE_ANSWER }

  # Scopes ::::::::::::::::::::::::::::::::::::::::::::::::::::::
  scope :list_view, -> { order(:created_at).reverse_order }
  scope :visible, -> { where(visible: true) }


  # Methods :::::::::::::::::::::::::::::::::::::::::::::::::::::
  def title_question_type
    ANSWER_TITLE_TYPE[question_type]
  end

  # @param [Object] question
  def self.title_answer(answer, question_type)
    if question_type == Question::MULTI_ANSWER
      Question::LIST_OPTIONS[answer.to_i]
    else
      answer
    end
  end

  private

  def question_save
    if question_type == MULTI_ANSWER
      self.answer = self.test_answer
    else
      self.answer = self.answer.downcase
    end
  end

end
