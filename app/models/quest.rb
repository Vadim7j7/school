class Quest < ActiveRecord::Base

  # Associations :::::::::::::::::::::::::::::::::::::::::::::::::
  belongs_to :user
  has_many :questions
  has_many :quest_finisheds
  accepts_nested_attributes_for :questions, :allow_destroy => true

  # Validates :::::::::::::::::::::::::::::::::::::::::::::::::::
  validates :caption, :description, :time_quest, presence: true
  validates_numericality_of :time_quest

end
