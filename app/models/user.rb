class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ROLE_STUDENT = 1
  ROLE_TEACHER = 2
  ROLE_TITLE = { ROLE_STUDENT => 'Student',
                 ROLE_TEACHER => 'Teacher' }
  ROLE_TITLE_OPTION = [ [ROLE_TITLE[ROLE_STUDENT], ROLE_STUDENT],
                        [ROLE_TITLE[ROLE_TEACHER], ROLE_TEACHER] ]

  attr_accessor :current_password

  # Associations :::::::::::::::::::::::::::::::::::::::::::::::::
  has_many :questions, dependent: :destroy
  has_many :student_answers, dependent: :destroy

  # Validates :::::::::::::::::::::::::::::::::::::::::::::::::::
  validates :first_name, presence: true
  validates :last_name, presence: true
  validate :valid_role

  # Scopes ::::::::::::::::::::::::::::::::::::::::::::::::::::::
  scope :student, -> { where(role: ROLE_STUDENT) }
  scope :teacher, -> { where(role: ROLE_TEACHER) }

  # Methods :::::::::::::::::::::::::::::::::::::::::::::::::::::

  private

  def valid_role
    if role != ROLE_STUDENT && role != ROLE_TEACHER
      errors.add(:role, 'Invalid role')
    end
  end
end
