class Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :quest
  belongs_to :question
end
