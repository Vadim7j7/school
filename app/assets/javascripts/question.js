(function(window, $) {

    "use-strict"

    // Generate option row
    function gen_option(option, ii, i) {
        var elTr, elTd1, elTd2, elTd3, elDiv, elLabel, elInput1, elInput2;
        elTr = window.document.createElement("tr");
        elTd1 = window.document.createElement("td");
        elTd2 = window.document.createElement("td");
        elTd3 = window.document.createElement("td");
        elDiv = window.document.createElement("div");
        elLabel = window.document.createElement("label");
        elInput1 = window.document.createElement("input");
        elInput2 = window.document.createElement("input");

        elTr.className = "table_row_question-type-multi";
        elTd1.innerHTML = option;
        elDiv.className = "radio";
        elInput1.className = "form-control";
        elInput1.setAttribute("placeholder", "Answer");
        elInput1.name = "quest[questions_attributes]["+ii+"][question_options_attributes]["+i+"][txt]";
        elInput2.type = "radio";
        elInput2.name = "quest[questions_attributes]["+ii+"][test_answer]";
        elInput2.value = i;

        elTd2.appendChild(elInput1);

        elLabel.appendChild(elInput2);
        elDiv.appendChild(elLabel);
        elTd3.appendChild(elDiv);

        elTr.appendChild(elTd1);
        elTr.appendChild(elTd2);
        elTr.appendChild(elTd3);

        return elTr;
    }

    // Generate question form
    function gen_question_form(i) {
        var randObjectId;
        randObjectId = parseInt(Math.random() * (9999999999 - 1111111111) + 9999999999);

        //

        var divPanel;
        divPanel = window.document.createElement("div");
        divPanel.className = "panel panel-default question-panel";
        divPanel.innerHTML =
            '<div class="panel-heading">Question - '+(i+1)+'</div> ' +
            '<div class="panel-body">' +
            '<div class="form-group">' +
            '<textarea class="form-control" rows="5" name="quest[questions_attributes]['+i+'][txt]" id="quest_questions_attributes_0_txt"></textarea>' +
            '</div>' +
            '<div class="form-group">' +
            '<select class="form-control selectpicker select_question_type" data-id="'+randObjectId+'" name="quest[questions_attributes]['+i+'][question_type]" id="quest_questions_attributes_0_question_type">' +
            '<option selected="selected" value="0">Question answer</option>' +
            '<option value="1">Multiple replies</option>' +
            '</select>' +
            '</div>' +
            '<div id="question-type-one_'+randObjectId+'" style="display: block;">' +
            '<div class="form-group">' +
            '<input class="form-control" rows="5" placeholder="Answer" type="text" name="quest[questions_attributes]['+i+'][answer]" id="quest_questions_attributes_0_answer">' +
            '</div>' +
            '</div>' +
            '<input id="question_type_'+randObjectId+'" type="hidden" value="0" name="quest[questions_attributes]['+i+'][question_type]">' +
            '<div id="question-type-multi_'+randObjectId+'" style="display: none;">' +
            '<table class="table">' +
            '<thead>' +
            '<tr>' +
            '<th>Option</th>' +
            '<th>Answer</th>' +
            '<th>Correct answer</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="question-type-multi-body_list_'+randObjectId+'" data-options="A|B|C|D|E|F|G" data-option-now="1">' +
            '<tr class="table_row_question-type-multi">' +
            '<td>A</td>' +
            '<td>' +
            '<input class="form-control" placeholder="Answer" type="text" name="quest[questions_attributes]['+i+'][question_options_attributes][0][txt]" id="quest_questions_attributes_0_question_options_attributes_0_txt">' +
            '</td>' +
            '<td>' +
            '<div class="radio">' +
            '<label>' +
            '<input type="radio" name="quest[questions_attributes]['+i+'][test_answer]" value="0" checked="">' +
            '</label>' +
            '</div>' +
            '</td>' +
            '</tr>' +
            '</tbody>' +
            '</table>' +
            '<button type="button" class="btn btn-primary button_add_option" data-id="'+randObjectId+'">Add option</button>&nbsp;' +
            '<button type="button" class="btn btn-danger button_remove_option" data-id="'+randObjectId+'">Remove last</button>' +
            '</div>' +
            '</div>';

        return divPanel;

        /*
         <div class="panel-heading">Question</div>
         <div class="panel-body">
         <div class="form-group">
         <textarea class="form-control" rows="5" name="quest[questions_attributes][0][txt]" id="quest_questions_attributes_0_txt"></textarea>
         </div>

         <div class="form-group">
         <select class="form-control selectpicker select_question_type" data-id="70183311271860" name="quest[questions_attributes][0][question_type]" id="quest_questions_attributes_0_question_type">
         <option selected="selected" value="0">Question answer</option>
         <option value="1">Multiple replies</option>
         </select>
         </div>

         <div id="question-type-one_70183311271860" style="display: block;">
         <div class="form-group">
         <input class="form-control" rows="5" placeholder="Answer" type="text" name="quest[questions_attributes][0][answer]" id="quest_questions_attributes_0_answer">
         </div>
         </div>

         <input id="question_type_70183311271860" type="hidden" value="0" name="quest[questions_attributes][0][question_type]">

         <div id="question-type-multi_70183311271860" style="display: none;">
         <table class="table">
         <thead>
         <tr>
         <th>Option</th>
         <th>Answer</th>
         <th>Correct answer</th>
         </tr>
         </thead>
         <tbody id="question-type-multi-body_list_70183311271860" data-options="A|B|C|D|E|F|G" data-option-now="1">
         <tr class="table_row_question-type-multi">
         <td>A</td>
         <td>
         <input class="form-control" placeholder="Answer" type="text" name="quest[questions_attributes][0][question_options_attributes][0][txt]" id="quest_questions_attributes_0_question_options_attributes_0_txt">
         </td>
         <td>
         <div class="radio">
         <label>
         <input type="radio" name="question[test_answer]" value="0" checked="">
         </label>
         </div>
         </td>
         </tr>
         </tbody>
         </table>
         <button type="button" class="btn btn-primary button_add_option" data-id="70183311271860">Add option</button>
         <button type="button" class="btn btn-danger button_remove_option" data-id="70183311271860">Remove last</button>
         </div>
         </div>
        */
    }

    function loadPage() {
        // Change select type question
        var selectQuestionType;
        selectQuestionType = $(".select_question_type");
        if (selectQuestionType.length > 0) {
            $(document).on("change", ".select_question_type", function() {
                var dataId;
                dataId = this.getAttribute("data-id");
                if (dataId) {
                    var selected, questionTypeOne, questionTypeMulti, questionType;
                    selected = $(this).find("option:selected").val();
                    questionTypeOne = $("#question-type-one_"+dataId);
                    questionTypeMulti = $("#question-type-multi_"+dataId);
                    questionType = $("#question_type_"+dataId);

                    if (questionTypeOne.length > 0 && questionTypeMulti.length > 0) {
                        if (selected == 0) {
                            questionTypeOne.show();
                            questionTypeMulti.hide();
                        } else {
                            questionTypeOne.hide();
                            questionTypeMulti.show();
                        }
                        questionType.val(selected);
                    }
                }
            });
        }
        // END

        // Create and remove option
        var buttonAddOption, buttonRemoveOption, wrapperQuestions;
        buttonAddOption = $(".button_add_option");
        buttonRemoveOption = $(".button_remove_option");
        wrapperQuestions = window.document.getElementById("wrapper_questions");
        if (buttonAddOption.length > 0 && buttonRemoveOption.length > 0) {
            // Add option to question
            //buttonAddOption.on("click", function() {
            $(document).on("click", ".button_add_option", function() {
                var optionId, viewOut;
                optionId = this.getAttribute("data-id");
                viewOut = window.document.getElementById("question-type-multi-body_list_"+optionId);
                if (!optionId || !viewOut) {return;}

                var dataOptions, nOption, newOption;
                dataOptions = viewOut.getAttribute("data-options").split('|');
                nOption = parseInt(viewOut.getAttribute("data-option-now"));

                if (nOption >= dataOptions.length) { return; }
                var nQuestion = parseInt(wrapperQuestions.getAttribute("data-option-now")) - 1;

                newOption = gen_option(dataOptions[nOption], nQuestion, nOption);
                viewOut.appendChild(newOption);
                viewOut.setAttribute("data-option-now", nOption+1);
            });

            // Remove option from question
            //buttonRemoveOption.on("click", function() {
            $(document).on("click", ".button_remove_option", function() {
                var optionId, viewOut;
                optionId = this.getAttribute("data-id");
                viewOut = window.document.getElementById("question-type-multi-body_list_"+optionId);
                if (!optionId || !viewOut) {return;}

                var dataOptions, nOption;
                dataOptions = viewOut.getAttribute("data-options").split('|');
                nOption = parseInt(viewOut.getAttribute("data-option-now"));

                if (nOption <= 1){ return; }
                var elsTr, lastTr;
                elsTr = viewOut.getElementsByTagName("tr");
                lastTr = elsTr[elsTr.length-1];

                lastTr.parentNode.removeChild(lastTr);

                viewOut.setAttribute("data-option-now", nOption-1);
            });
        }

        // Create and remove question
        var buttonAddQuestion, buttonRemoveQuestion;
        buttonAddQuestion = $(".button_add_question");
        buttonRemoveQuestion = $(".button_remove_question");
        if (buttonAddQuestion.length > 0 && buttonRemoveQuestion.length > 0) {
            buttonAddQuestion.on("click", function() {
                var newQuestion, nQuestion;
                nQuestion = parseInt(wrapperQuestions.getAttribute("data-option-now"));
                newQuestion = gen_question_form(nQuestion);
                wrapperQuestions.appendChild(newQuestion);

                wrapperQuestions.setAttribute("data-option-now", nQuestion+1);
            });
            buttonRemoveQuestion.on("click", function() {
                var nQuestion;
                nQuestion = parseInt(wrapperQuestions.getAttribute("data-option-now"));

                if (nQuestion <= 1) { return; }
                var questions, lastQuestion;
                questions = $(".question-panel");
                lastQuestion = questions[questions.length - 1];

                lastQuestion.parentNode.removeChild(lastQuestion);

                wrapperQuestions.setAttribute("data-option-now", nQuestion-1);
            });
        }
    }

    // (Hook) Turbolinks load script
    $(window.document).on("ready", loadPage);
    $(document).on('page:load', loadPage)

})(this, jQuery);