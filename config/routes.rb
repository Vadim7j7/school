Rails.application.routes.draw do
  root 'home#index'

  devise_for :users, controllers: { registrations: 'users/registrations' }

  scope :process do
    match '/run', to: 'process#run', via: [:get, :post], as: :process_run
    get '/finished', to: 'process#quest_finished', as: :process_quest_finished
  end

  scope :quests do
    get '/', to: 'quests#index', as: :quests
    get '/show/:id', to: 'quests#show', as: :quest_show
    get '/new', to: 'quests#new', as: :quest_new
    post '/new', to: 'quests#create', as: :quest_create
  end

end
