# Create Student
User.create(email: 'student_school@best-freedom.ru',
            first_name: 'Ivan',
            last_name: 'Ivanov',
            password: '929s41s',
            password_confirmation: '929s41s',
            role: User::ROLE_STUDENT)

# Create Teacher
u = User.create(email: 'teacher_school@best-freedom.ru',
            first_name: 'Petr',
            last_name: 'Petrov',
            password: 'cksdflks3',
            password_confirmation: 'cksdflks3',
            role: User::ROLE_TEACHER)
