# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151118172316) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "quest_id"
    t.integer  "question_id"
    t.string   "answer"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "true_or_false", default: false
  end

  add_index "answers", ["quest_id"], name: "index_answers_on_quest_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "quest_finisheds", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "quest_id"
    t.string   "time_spent"
    t.string   "percentage_true"
    t.string   "percentage_false"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "quest_finisheds", ["quest_id"], name: "index_quest_finisheds_on_quest_id", using: :btree
  add_index "quest_finisheds", ["user_id"], name: "index_quest_finisheds_on_user_id", using: :btree

  create_table "question_options", force: :cascade do |t|
    t.integer  "question_id"
    t.string   "txt"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "question_options", ["question_id"], name: "index_question_options_on_question_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "txt"
    t.string   "answer"
    t.integer  "question_type",      default: 0,    null: false
    t.boolean  "visible",            default: true
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "quest_id"
  end

  add_index "questions", ["quest_id"], name: "index_questions_on_quest_id", using: :btree
  add_index "questions", ["user_id"], name: "index_questions_on_user_id", using: :btree

  create_table "quests", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "caption"
    t.text     "description"
    t.integer  "time_quest"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "quests", ["user_id"], name: "index_quests_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role",                   default: 1,  null: false
    t.string   "first_name",                          null: false
    t.string   "last_name",                           null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "quests"
  add_foreign_key "answers", "users"
  add_foreign_key "quest_finisheds", "quests"
  add_foreign_key "quest_finisheds", "users"
  add_foreign_key "question_options", "questions"
  add_foreign_key "questions", "quests"
  add_foreign_key "questions", "users"
  add_foreign_key "quests", "users"
end
