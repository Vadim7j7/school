class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.text :txt
      t.string :answer
      t.integer :question_type, :null => false, :default => Question::ONE_ANSWER
      t.boolean :visible, :default => true

      t.timestamps null: false
    end
  end
end
