class CreateQuestionOptions < ActiveRecord::Migration
  def change
    create_table :question_options do |t|
      t.belongs_to :question, index: true, foreign_key: true
      t.string :txt

      t.timestamps null: false
    end
  end
end
