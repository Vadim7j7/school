class CreateQuestFinisheds < ActiveRecord::Migration
  def change
    create_table :quest_finisheds do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :quest, index: true, foreign_key: true
      t.string :time_spent
      t.string :percentage_true
      t.string :percentage_false

      t.timestamps null: false
    end
  end
end
