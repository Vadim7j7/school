class AddRoleToUser < ActiveRecord::Migration
  def change
    add_column :users, :role, :integer, :null => false, :default => User::ROLE_STUDENT
  end
end
