class AddTrueOrFalseToAnswer < ActiveRecord::Migration
  def change
    add_column :answers, :true_or_false, :boolean, default: false
  end
end
