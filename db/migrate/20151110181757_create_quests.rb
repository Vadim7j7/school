class CreateQuests < ActiveRecord::Migration
  def change
    create_table :quests do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :caption
      t.text :description
      t.integer :time_quest

      t.timestamps null: false
    end
  end
end
