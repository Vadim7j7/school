class AddQuestToQuestion < ActiveRecord::Migration
  def change
    add_reference :questions, :quest, index: true, foreign_key: true
  end
end
